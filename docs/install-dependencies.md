# Azure Tools Install

*If you already have all these tools installed, feel free to skip this tutorial.*

On your localhost, install the following thirdparty tools.

## Azure CLI

        # Dependencies
        sudo apt-get update
        sudo apt-get install ca-certificates curl apt-transport-https lsb-release gnupg

        # Software Signing Key
        curl -sL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null

        # Add Azure CLI Software Repository
        AZ_REPO=$(lsb_release -cs)
        echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | sudo tee /etc/apt/sources.list.d/azure-cli.list

        # Azure CLI Package
        sudo apt-get update
        sudo apt-get install azure-cli

### Azcopy

        # download azcopy
        # - the wget command should take only a few seconds to download.
        # - if the wget command does not work or takes too long, put the https link in your browser and download it to your preferred path
        cd /tmp/
        wget -O azcopy_v10.tar.gz https://aka.ms/downloadazcopy-v10-linux && tar -xf azcopy_v10.tar.gz --strip-components=1

        # remove any previous azcopy versions
        # -- continue with the instructions, even if you see `rm: cannot remove '/usr/bin/azcopy': No such file or directory`
        sudo rm /usr/bin/azcopy

        # move azcopy to bin path
        sudo cp azcopy /usr/bin/

        # add execution permissions
        sudo chmod +x /usr/bin/azcopy

        # remove downloaded azcopy
        rm azcopy azcopy_v10.tar.gz

## Terraform

        # Dependencies
        sudo apt-get install unzip wget

        # Terraform CLI package
        cd ~/Downloads/
        wget https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip
        unzip terraform_0.12.24_linux_amd64.zip
        sudo mv terraform /usr/local/bin/
        rm terraform_0.12.24_linux_amd64.zip

### TeamViewer (optional)

        # Download the teamviewer deb package
        cd /tmp/
        wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb

        # Install teamviewer
        # At the prompt Do you want to continue? [Y/n], type Y to continue the installation.
        sudo apt install ./teamviewer_amd64.deb

        # Remove the deb package
        rm teamviewer_amd64.deb
