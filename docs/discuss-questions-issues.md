# Common Questions

## Changing Terraform Files

You should become comfortable creating or updating terraform files.

- A simple next example you can try out is adding another VM terraform file with username/password setup.

Any changes to the terraform files, requires updating the terraform workspace

        # Dry-run: shows the user the azure deployment
        terraform plan


Apply the changes to the cloud

        # Apply the terraform setup to azure
        terraform apply

## Remove Terraform Created Resources from Azure

**WARNING: Be careful what resource group or resources you are destroying!!**

- Be careful not destroy another user's resources (**always check** the command line variable names or nested resource links).

**Example:** Remove an existing Virtual Machine:

        cd azurebooks/airlab/

        # this will destroy everything create  in the example terraform workspace
        terraform destroy

        # remove a specific terraform resource (example)
        # note: terraform does not remove linked resources, only the resource explicitly specified
        terraform destroy -target module.example.azurerm_linux_virtual_machine.ugv1

        # remove multiple terraform resources
        terraform destroy -target module.example.azurerm_linux_virtual_machine.ugv1 \
                -target module.example.azurerm_network_interface_security_group_association.ugv1 \
                -target module.example.azurerm_network_interface.ugv1

Always, verify on the Azure Portal that your resources have been destroyed.

- Sometimes, the above steps can result in errors. Or it might not report any errors, but the resource might still exist on the portal.
- Go to the Azure portal and directly remove the resource if its still exists.

**WARNING:** Synchronization errors can occur when removing azure resources directly on the portal, but without removing using terraform.

- If terraform does not find those resources on Azure it might not be able to sync its `terraform state` files correctly.
- Always remove the resources using terraform first. If there is a failure, then remove the resource on the azure portal website directly.

* * *

# Issues

**Permision denined**

        # ssh into your VM with the identify file specified
        ssh -i /home/$USER/.ssh/path/to/id_rsa [username]@[private IP]

**Too many authentication failures**

        ssh -o IdentitiesOnly=yes [username]@[private IP]

**SSH connection stuck**

        ssh -o MACs=hmac-sha2-256 [username]@[private IP]

If you ssh connection is still stuck, debug the issue by viewing the verbose connection log:

        ssh -v [username]@[private IP]

**Remote host identification has changed**

If you ssh into your VM and you see the following error (example):

        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        @    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
        Someone could be eavesdropping on you right now (man-in-the-middle attack)!
        It is also possible that a host key has just been changed.
        The fingerprint for the ECDSA key sent by the remote host is
        SHA256:61VpK3J5BABJa3JDbjPxtMPlnoPdMeZaOVavdpn3HT8.
        Please contact your system administrator.
        Add correct host key in /home/USERNAME/.ssh/known_hosts to get rid of this message.
        Offending ECDSA key in /home/USERNAME/.ssh/known_hosts:41
        remove with:
        ssh-keygen -f "/home/USERNAME/.ssh/known_hosts" -R "azure-uav1"
        ECDSA host key for azure-uav1 has changed and you have requested strict checking.
        Host key verification failed.

The warning is saying that you have previously `ssh-ed` into a host with the same IP, but a different machine.

Perform the first step, apply the `ssh-keygen` update:

        ssh-keygen -f "/home/USERNAME/.ssh/known_hosts" -R "azure-uav1"

Perform the next step, remove the previous host key in `know_hosts`:

        # Open the known_hosts, to the problematic line.
        # Example: /home/USERNAME/.ssh/known_hosts:41
        #   - the problamatic line is 41

        # STEP:  open the file
        gedit /home/USERNAME/.ssh/known_hosts

        # STEP: Go to line 41

        # STEP:  Remove the ENTIRE line

Perform the final step, ssh into the VM again:

        # Example, enter the Azure UAV1 VM
        ssh azure.uav1

        # When prompted:
        #   'Are you sure you want to continue connecting (yes/no)?'
        # STEP: Say 'yes'

You should not see the above error again and should be logged into the remote VM.

**Terraform Error Acquiring Lock**

If you have a corrupted terraform `state` file, you might end up with a acquiring lock error as such:

        Error locking state: Error acquiring the state lock: storage: service returned error: StatusCode=409, ErrorCode=LeaseAlreadyPresent, ErrorMessage=There is already a lease present.
        RequestId:3ea11a03-701e-0092-62a0-45c8e9000000
        Time:2020-06-18T18:46:11.9697526Z, RequestInitiated=Thu, 18 Jun 2020 18:46:11 GMT, RequestId=3ea11a03-701e-0092-62a0-45c8e9000000, API Version=2018-03-28, QueryParameterName=, QueryParameterValue=
        Lock Info:
        ID:        ac14e41a-4ca6-01e5-5f1c-36368320b3c5
        Path:      subtdeploy-statefile-container/workspaces/USERNAME/terraform.tfstate
        Operation: OperationTypePlan
        Who:       USERNAME@USER-HOSTNAME
        Version:   0.12.24
        Created:   2020-06-18 18:44:10.797134982 +0000 UTC
        Info:

        Terraform acquires a state lock to protect the state from being written
        by multiple users at the same time. Please resolve the issue above and try
        again. For most commands, you can disable locking with the "-lock=false"
        flag, but this is not recommended.


If you are the only user, using thee terraform `state` file, go ahead and force an unlock:

        terraform force-unlock <lock-id-guid>

**More discussion**

For solving ssh errors, it might be easier to setup an [ssh connection setup](https://www.digitalocean.com/community/tutorials/how-to-configure-custom-connection-options-for-your-ssh-client) in `~/.ssh/config`
