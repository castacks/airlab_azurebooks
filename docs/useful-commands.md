# Useful Azure Command Line (az) Commands

*Feel free to add more useful commands here.*

List your user account information:

      az account list

List all resource groups available:

      az group list -o table

List all resources. Example, for the `SubT` resource group:

      az resource list -g SubT -o table

List all resources, matching pattern. Example, for the `SubT` resource group:

      # template: az resource list -g SubT -o table | grep [pattern]
      # example, with pattern:
      az resource list -g SubT -o table | grep USERNAME

List the IPs Virtual Machines. Example, found in `SubT` resource group:

      az vm list-ip-addresses -g SubT -o table

List the IPs Virtual Machines, matching prefix. Example, found in `SubT` resource group:

      az vm list-ip-addresses -g SubT -o table | grep [pattern]

List the public IPs, matching prefix. Example found in `SubT` resource group:

      # template: az resource list -g SubT -o table | grep [pattern]
      # example, with pattern:
      az network public-ip list -g SubT -o table | grep USERNAME

Start Azure VM

      az vm start -g [resource group] -n [vm name] -n [vm name]

Stop Azure VM

      az vm stop -g [resource group] -n [vm name] -n [vm name]

# Useful Terraform Command Line (terraform) Commands

## Changing Terraform Files

You should become comfortable creating or updating terraform files.

- A simple next example you can try out is adding another VM terraform file with username/password setup.

Any changes to the terraform files, requires updating the terraform workspace

        # Dry-run: shows the user the azure deployment
        terraform plan

Apply the changes to the cloud

        # Apply the terraform setup to azure
        terraform apply

## Remove Terraform Created Resources from Azure

**WARNING: Be careful what resource group or resources you are destroying!!**

- Be careful not destroy another user's resources (**always check** the command line variable names or nested resource links).

**Example:** Remove an existing Virtual Machine:

        cd ~/deploy_ws/src/operations/deploy/azurebooks/subt/

        # this will destroy everything create  in the example terraform workspace
        terraform destroy

        # remove a specific terraform resource (example)
        # note: terraform does not remove linked resources, only the resource explicitly specified
        terraform destroy -target module.example.azurerm_linux_virtual_machine.ugv1

        # remove multiple terraform resources
        terraform destroy -target module.example.azurerm_linux_virtual_machine.ugv1 \
                -target module.example.azurerm_network_interface_security_group_association.ugv1 \
                -target module.example.azurerm_network_interface.ugv1

Always, verify on the Azure Portal that your resources have been destroyed.

- Sometimes, the above steps can result in errors. Or it might not report any errors, but the resource might still exist on the portal.
- Go to the Azure portal and directly remove the resource if its still exists.

**WARNING:** Synchronization errors can occur when removing azure resources directly on the portal, but without removing using terraform.

- If terraform does not find those resources on Azure it might not be able to sync its `terraform state` files correctly.
- Always remove the resources using terraform first. If there is a failure, then remove the resource on the azure portal website directly.
