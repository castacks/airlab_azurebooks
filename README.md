# Azurebooks: Azure Terraform

[TOC]

There are two operational tools available to use: `az` or `terraform`

- `az` is the [azure commandline interface](https://docs.microsoft.com/en-us/cli/azure/?view=azure-cli-latest).

- `terraform` is the [terraform command line interface](https://learn.hashicorp.com/terraform) to interact with `terraform` files found in `~/infrastructure/src/azurebooks`

# Getting Started

The below instructions should get you started on a basic `azure` setup.

You will need to go through a few tutorials to have a working system.

### 1. Install Third-Party Tool (Required)

**Instructions at:**

- [`docs/install-dependencies.md`](docs/install-dependencies.md)

This tutorial will setup the following:

- installs thirdparty tools such as `docker, ansible, terraform,` etc., required to setup Azure infrastructure.

### 2. Create Azure Setup

**Select your project's relevant instructions:**

- Airlab Tutorial Week: [`airlab-tutorial/README.md`](airlab-tutorial/README.md)

This tutorial will setup the following:

- Create the Azure infrastructure (networking, VPN, VMs, etc), for your project.

## 3. Verify the Cloud Infrastructure

**Verify** the cloud infrastructure was created. Go to the [portal.azure.com](https://portal.azure.com/#home) and search for your infrastructure setup.

- In the your project's resource group, search for your `azure username` to find your VMs.
- Verify you see your resources are created (like Virtual Machines, networking, etc) with your username prefix.


## 4. Useful Command Line Commands (Optional)

This tutorial will go over useful command line commands for the thridparty tools.

**Discussion at:** [`docs/useful-commands.md`](docs/discuss-questions-issues.md)

## 5. Common Questions and Issues (Optional)

This tutorial will go over common questions and issues that can occur during azure deployment.

**Discussion at:** [`docs/discuss-questions-issues.md`](docs/discuss-questions-issues.md)
