# // /////////////////////////////////////////////////////////////////////////////
# Setup the cloud provider
# // /////////////////////////////////////////////////////////////////////////////
provider "azurerm" {
  # The "feature" block is required for AzureRM provider 2.x.
  # If you're using version 1.x, the "features" block is not allowed.
  version = "~>2.0"
  features {}

  # setup credentials
  # -- please have these variables set in your ~/.bashrc (or ~/.zshrc)
  subscription_id             = var.subscription_id
  tenant_id                   = var.tenant_id

  # skip provider registration
  skip_provider_registration  = "true"
}

# // /////////////////////////////////////////////////////////////////////////////
# @brief Terraform Backend
# Note: Variables are not allowed, everything must be statically written.
# // /////////////////////////////////////////////////////////////////////////////
terraform {
  # setup the backend remote for maintaining state file storage

  backend "azurerm" {

    # existing storage account (make sure exists on azure)
    storage_account_name = "airlabterrastate"

    # existing storage container
    container_name       = "airlab-statefile-container"

    # resource group, for the storage account
    resource_group_name  = "airlab"

    # path to the statefiles on the remote backend storage container
    # !! -- PLEASE CHANGE THE USERNAME (azure username) -- !!
    key                  = "workspaces/tutorial/USERNAME/terraform.tfstate"
  }
}

# // /////////////////////////////////////////////////////////////////////////////
# Load the example modules
# // /////////////////////////////////////////////////////////////////////////////
module "example" {
  source = "./modules/"

  # // /////////////////////////////////////////////////////////////////////////////
  # General Resource & Network Settings
  # // /////////////////////////////////////////////////////////////////////////////

  # use existing resource group name
  user_defined_resource_group_name      = "airlab"

  # set the resource location
  resource_location                     = "eastus"

  # name prefix to be used for all resources
  resource_name_prefix                  = "${var.azure_username}"

  # tag prefix
  tag_name_prefix                       = "${var.azure_username}-tag"

  # // /////////////////////////////////////////////////////////////////////////////
  # General VM Settings
  # // /////////////////////////////////////////////////////////////////////////////

  # location of local ssh key to connect to remote VM
  #   -- PLEASE DO NOT CHANGE!! KEEP THE DEFAULT PATH!!
  vm_pub_ssh_key                        = "~/.ssh/azure/vpn/tutorial/vm_key.pub"

  # VM disk sizes (in GB)
  # !! -- PLEASE CHANGE THE VALUE TO YOUR PREFERENCE -- !!
  vm_cpu_disk_size                      = 64
  vm_gpu_disk_size                      = 100

  # VM instance types
  # !! -- PLEASE CHANGE THE VALUE TO YOUR PREFERENCE -- !!
  #   - choose 'Standard_F8s_v2' or 'Standard_F16s_v2' for a cpu VM
  #   - choose 'Standard_NC6' for a gpu VM
  #   - enable or disable creating CPU or GPU VMs
  vm_cpu_instance                       = "Standard_F8s_v2"
  vm_gpu_instance                       = "Standard_NC6"

  # Enable VM Creation
  # !! -- PLEASE CHANGE THE VALUE TO YOUR PREFERENCE -- !!
  vm_cpu_create_vm                      = true
  vm_gpu_create_vm                      = false

}
