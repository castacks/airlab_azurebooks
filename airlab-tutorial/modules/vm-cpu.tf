# Virtual Network Interface -- connect VMs to network & security setup
resource "azurerm_network_interface" "vm-cpu" {
  # name of NIC
  name                        = "${var.resource_name_prefix}-NIC-vm-cpu"

  # resource group
  resource_group_name         = var.user_defined_resource_group_name

  # region location
  location                    = var.resource_location

  # toggle creation of a resource
  count                       = var.vm_cpu_create_vm ? 1 : 0

  ip_configuration {
    # name of NIC configuration
    name                          = "${var.resource_name_prefix}-NIC-vm-cpu-configuration"

    # subnet configuration for this NIC
    subnet_id                     = "/subscriptions/b1e974e5-8229-44c3-a472-235a580d611a/resourceGroups/airlab/providers/Microsoft.Network/virtualNetworks/airlab-tutorial-vnet/subnets/airlab-tutorial-subnet"

    # private dynamic ip allocation method
    private_ip_address_allocation = var.ip_alloc

    # private static  ip allocation method
    # private_ip_address_allocation = "Static"

    # private static ip address
    # private_ip_address            = "10.3.1.11"
  }

  tags = {
    environment = var.tag_name_prefix
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "vm-cpu" {
  # NIC interface id
  network_interface_id      = azurerm_network_interface.vm-cpu[count.index].id

  # Security Rules
  network_security_group_id = "/subscriptions/b1e974e5-8229-44c3-a472-235a580d611a/resourceGroups/airlab/providers/Microsoft.Network/networkSecurityGroups/airlab-tutorial-security-group"

  # toggle creation of a resource
  count                     = var.vm_cpu_create_vm ? 1 : 0
}

# Create virtual machine -- vm-cpu
resource "azurerm_linux_virtual_machine" "vm-cpu" {

  # name of vm
  name                  = "${var.resource_name_prefix}-vm-cpu"

  # resource group
  resource_group_name   = var.user_defined_resource_group_name

  # region location
  location              = var.resource_location

  # NIC interface id
  network_interface_ids = [azurerm_network_interface.vm-cpu[count.index].id]

  # toggle creation of a resource
  count                 = var.vm_cpu_create_vm ? 1 : 0

  # == VM instance Settings ==

  # instance type
  size                  = var.vm_cpu_instance

  # OS disk setup
  os_disk {
    name                    = "${var.resource_name_prefix}-vm-cpu-os-disk"
    caching                 = "ReadWrite"
    storage_account_type    = "Standard_LRS"
    disk_size_gb            = var.vm_cpu_disk_size
  }

  # VM image setup
  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  # == User Access Settings ==

  computer_name  = var.vm_cpu_hostname
  admin_username = var.vm_default_username
  # admin_password = var.vm_default_password

  # only allow ssh key connection
  disable_password_authentication = true

  # ssh connection configurations
  admin_ssh_key {
    username       = var.vm_default_username
    public_key     = file(var.vm_pub_ssh_key)
  }

  tags = {
    environment = var.tag_name_prefix
  }
}
