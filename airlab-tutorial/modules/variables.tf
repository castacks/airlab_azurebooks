# // /////////////////////////////////////////////////////////////////////////////
# resource & network varibles
# // /////////////////////////////////////////////////////////////////////////////

variable "user_defined_resource_group_name" {
  description = "default name prefix for resources"
  type = string
}

variable "resource_name_prefix" {
  description = "default name prefix for resources"
  type = string
}

variable "resource_location" {
  description = "default location of azure resources"
  type = string
  default = "eastus"
}

variable "ip_alloc" {
  description = "compute allocation method for public IP "
  type = string
  default = "Dynamic"
}

# // /////////////////////////////////////////////////////////////////////////////
# VM Variables
# // /////////////////////////////////////////////////////////////////////////////

variable "vm_pub_ssh_key" {
  description = "Location of the public ssh key on the local computer to connect to remote VM "
  type = string
}

variable "vm_default_password" {
  description = "Default password of a VM, please change on entry!"
  type = string
  default = "Password1234!"
}

variable "vm_default_username" {
  description = "Username of tutorial VM"
  type = string
  default = "airlab"
}


# /// Tutorial VM -- CPU

variable "vm_cpu_hostname" {
  description = "Hostname of tutorial VM"
  type = string
  default = "az-tutorial-cpu"
}

variable "vm_cpu_disk_size" {
  description = "tutorial cpu disk size"
  type = number
  default = 30
}

variable "vm_cpu_instance" {
  description = "Tutorial VM Instance Type"
  type = string
  default = "Standard_F8s_v2"
}

variable "enable_vm_cpu" {
  description = "toggle (enable or disable) for creating Tutorial VMs with GPU"
  type = bool
  default = false
}

# /// Tutorial VM -- GPU

variable "vm_gpu_hostname" {
  description = "Hostname of tutorial VM"
  type = string
  default = "az-tutorial-gpu"
}

variable "vm_gpu_disk_size" {
  description = "tutorial gpu disk size"
  type = number
  default = 30
}

variable "vm_gpu_instance" {
  description = "Tutorial GPU VM Instance Type"
  type = string
  default = "Standard_F8s_v2"
}

variable "enable_vm_gpu" {
  description = "toggle (enable or disable) for creating Tutorial VMs with GPU"
  type = bool
  default = false
}


# // /////////////////////////////////////////////////////////////////////////////
# resource creation toggles
# // /////////////////////////////////////////////////////////////////////////////

variable "vm_cpu_create_vm" {
  description = "toggle (enable or disable) for creating VMs for basic robot simulation"
  type = bool
  default = true
}

variable "vm_gpu_create_vm" {
  description = "toggle (enable or disable) for creating VMs for basic robot simulation"
  type = bool
  default = true
}

# // /////////////////////////////////////////////////////////////////////////////
# terraform tags
# // /////////////////////////////////////////////////////////////////////////////
variable "tag_name_prefix" {
  description = "example tag name"
  type = string
}
