# Terraform: Airlab Tutorial Week

This terraform example will create Virtual Machines, Networking and VPN setup on Azure.

- You will be able to ssh into the VM (using the private IP) over VPN.

**Things to keep in mind:**

- **All terraform commands must be done in the `azurebooks/airlab-tutorial` directory workspace**.

- The `azure username`, is the *user name* found before your `@...hotmail.onmicrosoft.com` azure account email.

- The `azure resource group` is `airlab`.

- Please **keep the default paths** as discussed in the readme instructions.

  - Some of these paths are hard-coded in scripts. If you do not wish to use these paths, please notify the maintainer to discuss which hard-coded values must be changed as variables.

- We have a smaller VM quota limit for the GPU VMs.
    - If you cannot create a GPU VM, please notify the maintainer, to make a request to increase the quota limits.
    - Or, change the variable `vm_gpu_create_vm` in the `main.tf` (explained later) to `false`.

## Terraform Project Prerequisites

**Azure CLI Initial Login:**

        # az login will prompt a browser window. Enter your user credentials to login.
        az login

**Find your Subscription and Tenant Ids**

        # List the ids
        az account list

        # you will see the following output:
        [
        {
            "cloudName": "AzureCloud",
            "homeTenantId": "...",
            "id": "b1e974e5-8229-44c3-a472-235a580d611a",
            "isDefault": true,
            "managedByTenants": [],
            "name": "Microsoft Azure Sponsorship",
            "state": "Enabled",
            "tenantId": "...",
            "user": {
              "name": "...",
              "type": "user"
            }
        }
        ]

Your Subscription Id is:

        "id": "..."

Your Tenant id is:

        "homeTenantId": "..."

**Add your environment variables:**

        # Open bashrc or zshrc
        gedit ~/.bashrc

        # write the subscription id
        export TF_VAR_subscription_id=SUBSCRIPTION_ID_GOES_HERE
        # write the tenant id
        export TF_VAR_tenant_id=TENANT_ID_GOES_HERE
        # write your azure user name
        export TF_VAR_azure_username=AZURE_USER_NAME_GOES_HERE

**Export your environment variables in your current terminal:**

Source your `bashrc` or `zshrc` directly:

        # source bashrc directly
        source ~/.bashrc

        # source zshrc directly
        source ~/.zshrc

- *If the above does not work, double check for any typos or open up a new terminal.*

## Terraform Project

### Connect to Airlab Azure VPN

The below instructions can also be found on [azure tutorials](https://docs.microsoft.com/en-us/azure/vpn-gateway/point-to-site-vpn-client-configuration-azure-cert) for reference.

**Install Dependency Libraries**

      sudo apt-get update
      sudo apt-get install strongswan strongswan-pki libstrongswan-extra-plugins network-manager-strongswan

**Create the User Certificate**

        # == Download the CA Certificate from Perceptron ==
        mkdir -p ~/.ssh/azure/vpn/
        cd ~/.ssh/azure/vpn/

        # Copy over the VPN certificates
        # -- notive, this is your perceptron account username. Not the Azure account username.
        # -- these certificates also exist on azure storage: `airlabgeneralstorage`, but will require using a SAS key to use azcopy.
        scp -vr USERNAME@perceptron.ri.cmu.edu:/project/azure/tutorial/ .

        # == Create the User Certificate ==

        # Go to the directory with the CA certificates
        cd ~/.ssh/azure/vpn/tutorial

        # Change 'password' to something more secure
        export PASSWORD="password"
        # Change 'username' to your username (change to your azure username)
        export USERNAME="client"

        # generate the user certificate
        ipsec pki --gen --outform pem > "${USERNAME}Key.pem"
        ipsec pki --pub --in "${USERNAME}Key.pem" | ipsec pki --issue --cacert caCert.pem --cakey caKey.pem --dn "CN=${USERNAME}" --san "${USERNAME}" --flag clientAuth --outform pem > "${USERNAME}Cert.pem"

        # generate the p12 bunder
        openssl pkcs12 -in "${USERNAME}Cert.pem" -inkey "${USERNAME}Key.pem" -certfile caCert.pem -export -out "${USERNAME}.p12" -password "pass:${PASSWORD}"

**Download the VPN Client**

      # go to a ssh folder to contain your vpn keys
      cd ~/.ssh/azure/vpn/tutorial

      # Get the vpn client, this will output a https link, please remember it!
      az network vnet-gateway vpn-client generate --name airlab-tutorial-vnet-gateway --processor-architecture x86 --resource-group airlab

      # download the client (without brackets)
      # if the wget command does not work, put the https link (from the previous step) in your browser and download it to '~/.ssh/azure/vpn' location
      wget --directory-prefix=. [https path from previous command WITHOUT QUOTES ]

      # unzip the vpn client package
      #   - its okay to ignore the warnings '1 archive had warnings but no fatal errors.'
      unzip -j -d client-download [downloaded.zip]

      # == Connect to the VPN ==

      # Get the VPN server DNS, copy the name between '<VpnServer>' tags
      cd client-download
      grep -rni VpnServer

**Setup Networking Manager**

To setup your localhost Network Manager, please follow the [instructions here](https://docs.microsoft.com/en-us/azure/vpn-gateway/point-to-site-vpn-client-configuration-azure-cert#install).

Summary of above link (please use the link):

- Open the Network Manager Ubuntu GUI

- Add a new `VPN` connection, make sure it is the `IPsec/IKEv2 (strongswan)` connection

- Add the `[client]Cert.pem`, `[client]Key.pem` and the VPN server DNS name between `<VpnServer>` tags, with the `.vpn.azure.com` suffix (from the previous step).

- Select `Request an inner IP address`

- Select the VPN connection

- Apply the VPN connection

- Select the folder icon at the end of the Certificate field, browse to the Generic folder, and select the VpnServerRoot file.

Your end result should look like:

![Network Manager](../docs/images/airlab-vpn-example.png)

### Create the VM SSH Keys

**Azure SSH Keys**

- Generate ssh keys for azure vms:

        cd ~/.ssh/azure/vpn/tutorial
        ssh-keygen

    - Answer the prompts from `ssh-keygen` as shown below:

            Enter file in which to save the key (/home/<USER-NAME>/.ssh/id_rsa): /home/<USER-NAME>/.ssh/azure/vpn/tutorial/vm_key
            Enter passphrase (empty for no passphrase):

    - **DO NOT ENTER A PASSPHRASE on `ssh-keygen`! LEAVE IT BLANK.**
    - Replace `<USER-NAME>` with your actual localhost username

## Personalize the Terraform Main File

**Top Level Workspace**

        # go to the terraform workspace
        cd ~/infrastructure_ws/src/azurebooks/airlab-tutorial

        # edit the main terraform configuration file
        gedit ~/infrastructure_ws/src/azurebooks/airlab-tutorial/main.tf

**Please change the variables** in the `main.tf` file that have the comment: `"PLEASE CHANGE THE VALUE TO YOUR PREFERENCE"`

- **Change** `terraform:key` to include your azure username.

- **Change** `vm_cpu_create_vm` or `vm_gpu_create_vm` to your preference.

- **Change** `vm_cpu_disk_size` or `vm_cpu_instance` to your preference.

- If you do not like the defaults for `resource_name_prefix` or `tag_name_prefix`, feel free to change to your preference.

    - **For your first tutorial setup, please keep the defaults.** You can change it after you have done the tutorial.
    - The default uses your azure username as the prefix.

- For the `vm_pub_ssh_key` path, please make sure this key path exists on your localhost. This is the ssh key used to access the Azure VMs.

### Deploy Your Azure Setup

**Top Level Workspace**

        # Go to the terraform workspace
        cd ~/infrastructure_ws/src/azurebooks/airlab-tutorial

**Initialize the terraform workspace**

        terraform init

**Dry-run the terraform deployment**

        # Dry runs the azure deployment
        terraform plan

- **Errors:** if you see `"Error: Initialization required. Please see the error message above."`, please do `terraform init` again.

**Apply the terraform infrastructure setup to Azure**

        # will create all the resources on azure
        terraform apply

**Connect to your VM using VPN**

- **To find the IP:**

    - Go to the Azure Portal Website

    - Or, run the command below:

            az vm list-ip-addresses -g airlab -o table | grep [your azure username]

- **To access the VM:**

        ping [private IP]

        # ssh into your VM
        ssh airlab@[private IP]

        # Or if the above does not work, try the following ssh command:
        ssh -o IdentitiesOnly=yes  -i ~/.ssh/azure/vpn/tutorial/vm_key airlab@[private IP]

    - The default VM username is: `airlab`

### Final Comments

You should now be able to ssh into the Azure VM.
- If you had any issues, please see the below **Issues** section.

Please take a look at some useful `az` or `terraform` commands, found here: [azurebooks/docs/useful-commands.md](../docs/useful-commands.md)

***

## Issues

The default VM username is: `airlab`

**Permision denined**

        # ssh into your VM with the identify file specified
        ssh -i /home/$USER/.ssh/path/to/id_rsa airlab@[private IP]

**Too many authentication failures**

        ssh -o IdentitiesOnly=yes airlab@[private IP]

**SSH connection stuck**

        ssh -o MACs=hmac-sha2-256 airlab@[private IP]

If you ssh connection is still stuck, debug the issue by viewing the verbose connection log:

        ssh -v airlab@[private IP]

**Remote host identification has changed**

If you ssh into your VM and you see the following error (example):

        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        @    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
        Someone could be eavesdropping on you right now (man-in-the-middle attack)!
        It is also possible that a host key has just been changed.
        The fingerprint for the ECDSA key sent by the remote host is
        SHA256:61VpK3J5BABJa3JDbjPxtMPlnoPdMeZaOVavdpn3HT8.
        Please contact your system administrator.
        Add correct host key in /home/USERNAME/.ssh/known_hosts to get rid of this message.
        Offending ECDSA key in /home/USERNAME/.ssh/known_hosts:41
        remove with:
        ssh-keygen -f "/home/USERNAME/.ssh/known_hosts" -R "azure-uav1"
        ECDSA host key for azure-uav1 has changed and you have requested strict checking.
        Host key verification failed.

The warning is saying that you have previously `ssh-ed` into a host with the same IP, but a different machine.

Perform the first step, apply the `ssh-keygen` update:

        ssh-keygen -f "/home/USERNAME/.ssh/known_hosts" -R "azure-uav1"

Perform the next step, remove the previous host key in `know_hosts`:

        # Open the known_hosts, to the problematic line.
        # Example: /home/USERNAME/.ssh/known_hosts:41
        #   - the problamatic line is 41

        # STEP:  open the file
        gedit /home/USERNAME/.ssh/known_hosts

        # STEP: Go to line 41

        # STEP:  Remove the ENTIRE line

Perform the final step, ssh into the VM again:

        # Example, enter the Azure UAV1 VM
        ssh azure.uav1

        # When prompted:
        #   'Are you sure you want to continue connecting (yes/no)?'
        # STEP: Say 'yes'

You should not see the above error again and should be logged into the remote VM.

**Terraform Error Acquiring Lock**

If you have a corrupted terraform `state` file, you might end up with a acquiring lock error as such:

        Error locking state: Error acquiring the state lock: storage: service returned error: StatusCode=409, ErrorCode=LeaseAlreadyPresent, ErrorMessage=There is already a lease present.
        RequestId:3ea11a03-701e-0092-62a0-45c8e9000000
        Time:2020-06-18T18:46:11.9697526Z, RequestInitiated=Thu, 18 Jun 2020 18:46:11 GMT, RequestId=3ea11a03-701e-0092-62a0-45c8e9000000, API Version=2018-03-28, QueryParameterName=, QueryParameterValue=
        Lock Info:
        ID:        ac14e41a-4ca6-01e5-5f1c-36368320b3c5
        Path:      subtdeploy-statefile-container/workspaces/USERNAME/terraform.tfstate
        Operation: OperationTypePlan
        Who:       USERNAME@USER-HOSTNAME
        Version:   0.12.24
        Created:   2020-06-18 18:44:10.797134982 +0000 UTC
        Info:

        Terraform acquires a state lock to protect the state from being written
        by multiple users at the same time. Please resolve the issue above and try
        again. For most commands, you can disable locking with the "-lock=false"
        flag, but this is not recommended.


If you are the only user, using thee terraform `state` file, go ahead and force an unlock:

        terraform force-unlock <lock-id-guid>

**Cannot Create GPU Azure VM**

We have a smaller VM quota limit for the GPU VMs.

- If you cannot create a GPU VM, please notify the maintainer, to make a request to increase the quota limits.
- Or, change the variable `vm_gpu_create_vm` in the `main.tf` to `false`.


**More Issues Discussion:**

Please take a look at the full list of issues, found here: [azurebooks/docs/useful-commands.md](../docs/discuss-questions-issues.md)

* * *

## Remote Desktop

The airlab tutorial VM has remote desktop port enabled.

### Install desktop enviornment (remote host VM)

**On the remote VM do the commands:**

        # Please, make sure you are on the Azure VM, when running the below commands:
        sudo apt-get update
        sudo apt-get -y install xfce4
        sudo apt-get -y install xrdp
        sudo systemctl enable xrdp
        echo xfce4-session >~/.xsession
        sudo service xrdp restart

### Create a VM password (remote host VM)

`rdp` requires a password. There is no password setup for the default user.

  - The next tutorials will have the `ansible` scripts set a default password in the VMs for you.

Or, set the `airlab-tutorial` user's password manually. **On the remote VM do the commands:**

        # please, make sure you are on the Azure VM, when running the below commands:
        sudo su
        passwd airlab
        exit

### RDP client (localhost)

You can create your own rdp script: see `utils/sysadmin/example-remote-desktop.bash` as a template example.

Or you can use a custom infrastructure script. **On your localhost do the commands:**

        # install rdp client dependency
        sudo apt-get install rdesktop

        # connect to rdp server
        # - please make sure that 'my-rdp-window-name' is one word (i.e. no spaces)
        infra-azure-rdp --title [my-rdp-window-name] --host [VM private IP] --user airlab --pass [VM Password]

### Issues

`rdp` service needs to be restarted when the Azure VM starts:

- Restart the service manually in the VM:

        sudo systemctl enable xrdp
        echo xfce4-session >~/.xsession
        sudo service xrdp restart
